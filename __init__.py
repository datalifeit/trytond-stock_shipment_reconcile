# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .stock import (Configuration, ConfigurationSequence,
    Move, ShipmentReconciliation, ShipmentReconciliationLine,
    ShipmentReconcile, ShipmentReconcileStart)


def register():
    Pool.register(
        Configuration,
        ConfigurationSequence,
        ShipmentReconciliation,
        ShipmentReconciliationLine,
        Move,
        ShipmentReconcileStart,
        module='stock_shipment_reconcile', type_='model')
    Pool.register(
        ShipmentReconcile,
        module='stock_shipment_reconcile', type_='wizard')
