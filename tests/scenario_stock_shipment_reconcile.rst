=================================
Stock Shipment Reconcile Scenario
=================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules


Install stock_shipment_reconcile::

    >>> config = activate_modules('stock_shipment_reconcile')

